#/bin/sh

/bin/rm *dat INPUT_* *xyz *XYZ IAO_FOCK SAO
/bin/rm -rf plams_workdir* __pycache__ RESTART_ADF RESTART_AMS *.out *.t15 *rkf*

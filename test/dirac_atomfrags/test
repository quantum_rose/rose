#!/usr/bin/env python

# provides os.path.join
import os

# provides exit
import sys

# we make sure we can import runtest and runtest_config
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

# we import essential functions from the runtest library
from runtest import version_info, get_filter, cli, run

# this tells runtest how to run your code
from runtest_config import configure

# we stop the script if the major version is not compatible
assert version_info.major == 2

# construct a filter list which contains two filters
f = [
    get_filter(from_string='Partial charges of fragments',
               num_lines     = 10,
               rel_tolerance=1.0e-5),
    get_filter(from_string='Recanonicalized virtual energies of fragments',
               to_string=' Starting TESTS',
               abs_tolerance=1.0e-5),
    get_filter(re = 'Hartree-Fock energy',
               rel_tolerance   = 1.0e-8)
    ]

# invoke the command line interface parser which returns options
options = cli()

ierr = 0
program = 'dirac'
charge = 0
for mol in ['HCN', 'ethylene']:
   # the run function runs the code and filters the outputs
   ierr += run(options,
               configure,
               input_files=[program,mol,charge],
               filters={'stdout': f})

sys.exit(ierr)
